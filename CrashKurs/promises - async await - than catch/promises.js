/*  JS ist asyncron */
// setTimeout( () => {
//     console.log("Hallo nach 2 sek")
// }, 2000 );  // Wird zuerst ausgeführt, aber im log erscheint es erst 2sek später.

// console.log("Hallo");

/* Wie gelingt eine synchronisierung? - Promises */
const greeting = new Promise((resolve, reject) => {
    setTimeout( () => {
        console.log("Hallo nach 2 sek");
        resolve("Erledigt");
    }, 2000 );  // Wird zuerst ausgeführt, aber im log erscheint es erst 2sek später.
}); 

const startGreting = () => {
    greeting
    .then((result) => {
        console.log(result);
        console.log("Hallo");
    }).catch(() => {})
};

const startGretingOld = async () => {   // Analog
    try {
        const result = await greeting;
        console.log(result);
        console.log("Hallo");
    } catch (error) {
        // Fehler behandeln
    }
};

startGreting();