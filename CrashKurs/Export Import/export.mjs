const cube = (x) => x * x * x;

const PI = Math.PI;

// export { cube, PI } // Benannter export
export default { cube, PI } // Benannter export