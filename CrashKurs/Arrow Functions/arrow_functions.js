// const square = function(a) {
//     return a * a;
// };

// const square = (a) => {
//     return a * a;
// };

// const square = (a) => a * a;

// console.log(square(5));

// Klassische weise und arrow weise sind oft nicht identisch, obwohl sie das gleiche tun!

const car = {
    model: "Fiesta",
    manufacturer: "Ford",
    // fullName: function() {
    //     return `${this.manufacturer} ${this.model}`;
    // }
    
    fullNameWithArrow: () => {
        console.log(this);
        return `${this.manufacturer} ${this.model}`;
    },

    fullName() {
        console.log(this.fullNameWithArrow());
        console.log(this);
        return `${this.manufacturer} ${this.model}`;
    }
};

console.log(car.fullName());