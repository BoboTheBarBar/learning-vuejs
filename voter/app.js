/* Hier konfigurieren wir die view component */
const app = Vue.createApp({
  data: function () {
    return {
      submissions: submissions, // aus seed.js
    };
  },
});

/* Liefert eine Instanz zur root-component zurück */
const vm = app.mount("#app"); // viewmount
