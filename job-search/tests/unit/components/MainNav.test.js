import { mount } from "@vue/test-utils";

import MainNav from "@/components/MainNav.vue";

describe("MainNav", () => {
  it("displays the compony name", () => {
    const wrapper = mount(MainNav);
    expect(wrapper.text()).toMatch("Barbar Careers");
  });

  it("displays menu items for navigation", () => {
    const wrapper = mount(MainNav);
    const navigationMenuItems = wrapper.findAll(
      "[data-test='main-nav-list-item']"
    ); // [] -> search for an specific attribute
    const navigationMenuTexts = navigationMenuItems.map((item) => item.text());
    expect(navigationMenuTexts).toEqual([
      "Teams",
      "Locations",
      "Life at Barbar Corp",
      "How we hire",
      "Students",
      "Jobs",
    ]);
  });

  describe("when user ist logged out", () => {
    it("prompts user to sign in", () => {
      const wrapper = mount(MainNav);
      const logginButton = wrapper.find('[data-test="login-button"]');
      expect(logginButton.exists()).toBe(true);
    });
  });

  describe("when user ist logged in", () => {
    it("prompts user to sign in", async () => {
      const wrapper = mount(MainNav);
      let profileImage = wrapper.find('[data-test="profile-image"]');
      expect(profileImage.exists()).toBe(false);

      const logginButton = wrapper.find('[data-test="login-button"]');
      await logginButton.trigger("click");
      profileImage = wrapper.find('[data-test="profile-image"]');

      expect(profileImage.exists()).toBe(true);
    });
  });
});
